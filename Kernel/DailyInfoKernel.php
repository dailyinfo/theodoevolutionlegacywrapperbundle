<?php

/**
 * The namespace must be defined this way to allow to define the
 * get_instance function later (see the bottom of the file)
 */
namespace Theodo\Evolution\Bundle\LegacyWrapperBundle\Kernel {

    use Symfony\Component\DependencyInjection\ContainerInterface;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Theodo\Evolution\Bundle\LegacyWrapperBundle\Autoload\LegacyClassLoaderInterface;
    use Theodo\Evolution\Bundle\LegacyWrapperBundle\Exception\CodeIgniterException;

    class DailyInfoKernel extends LegacyKernel
    {
        /**
         * @var ContainerInterface
         */
        private $container;

        /**
         * {@inheritdoc}
         */
        public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
        {
            $doc_root = $this->getRootDir().'/htdocs';
            $session = $request->getSession();
            if ($session->isStarted()) {
                $session->save();
            }

            $app_bootstrap_output = ob_get_clean();

            $response = new Response();

            global $CFG, $RTR, $BM, $EXT, $CI, $URI, $OUT;

            ob_start();
            $this->cwd = getcwd();
            chdir($doc_root);
            require($this->getRootDir().'/htdocs/routing.php');

            $content = ob_get_clean();

//            // Restore the Symfony2 error handler
//            restore_error_handler();

            // Restart the Symfony 2 session
            $session->migrate();

            if (404 !== $response->getStatusCode()) {
//                $response->setContent($OUT->get_output());
                $response->setContent($content);
            }

            // restore original directory
            chdir($this->cwd);

            return $response;
        }

        /**
         * {@inheritdoc}
         */
        public function boot(ContainerInterface $container)
        {
            if (empty($this->options)) {
                throw new \RuntimeException('You must provide options for the CodeIgniter kernel.');
            }

            $this->container = $container;

            // Define global variables
            global $templateOptions;
            // extract global request variables
            // session start ?
            // Define application paths
            // Require legacy bootstrap file
            ob_start();
            require($this->getRootDir().'/config/global_prefix.php');


//            // Defines constants as it is done in the index.php file of your CodeIgniter project.
////            define('ENVIRONMENT', $this->options['environment']);
////            define('CI_VERSION', $this->options['version']);
////            define('CI_CORE', $this->options['core']);
//
//            // The name of the front controller
//            define('SELF', trim($container->get('request_stack')->getCurrentRequest()->getBaseUrl(), '/'));
//
//            // The PHP file extension
//            // this global constant is deprecated but used in some third party modules of CodeIgniter
//            define('EXT', '.php');
//
//            // Path to the system folder
//            $systemFolderPath = $this->getRootDir() . '/system/';
//            if (array_key_exists('system', $this->options)) {
//                $systemFolderPath = $this->options['system'];
//            }
//
//            define('BASEPATH', str_replace("\\", "/", realpath($systemFolderPath) . '/'));
//
//            // Path to the front controller (this file)
//            define('FCPATH', $container->getParameter('kernel.root_dir') . '/../web');
//
//            // Name of the "system folder"
//            define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));
//
//            // The path to the "application" folder
//            $applicationPath = $this->getRootDir() . '/application/';
//            if (array_key_exists('application', $this->options)) {
//                $applicationPath = $this->options['application'];
//            }
//
//            define('APPPATH', realpath($applicationPath) . '/');
//
//            // The path to the "sparks" folder
//            define('SPARKPATH', $this->getRootDir() . '/sparks/');
//
//
//            if (empty($this->classLoader)) {
//                throw new \RuntimeException('You must provide a class loader to the CodeIgniter kernel.');
//            }
//
//            if (!$this->classLoader->isAutoloaded()) {
//                $this->classLoader->autoload();
//            }

            $this->isBooted = true;
        }

        /**
         * Return the name of the kernel.
         *
         * @return string
         */
        public function getName()
        {
            return 'codeigniter';
        }

        /**
         * @return \Symfony\Component\DependencyInjection\ContainerInterface
         */
        public function getContainer()
        {
            return $this->container;
        }
    }
}

namespace {
    function &get_instance()
    {
        return \CI_Controller::get_instance();
    }
}